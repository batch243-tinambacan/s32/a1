
	let http = require("http");
	const port = 4000;

	http.createServer(function(request,response){

		if(request.url == "" && request.method == "GET"){

			response.writeHead(200,{'Content-Type': 'text/plain'});
			response.end("Welcome to booking system!");
		}

		if(request.url == "/profile" && request.method == "GET"){

			response.writeHead(200,{'Content-Type': 'text/plain'});
			response.end("Welcome to your profile!");
		}

		if(request.url == "/courses" && request.method == "GET"){

			response.writeHead(200,{'Content-Type': 'text/plain'});
			response.end("Here is our courses available");
		}


		else if (request.url == "/addcourse" && request.method == "POST"){
			
			response.writeHead(200,{"Content-Type":"text/plain"});
			response.end("Add a course to our resources");
		}

	}).listen(4000);

	console.log('Server successfully running at localhost: 4000')